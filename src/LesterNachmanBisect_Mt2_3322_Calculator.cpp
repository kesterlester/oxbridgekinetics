// Source file for the Oxbridge Stransverse Mass Library -- oxbridgekinetics.
// See http://www.hep.phy.cam.ac.uk/~lester/mt2/index.html
// Authors: Christopher Lester


#include "Mt2/LesterNachmanBisect_Mt2_3322_Calculator.h"
#include "Mt2/Mt2Units.h"

#include "lester_mt2_bisect.h"


namespace Mt2 {

  LesterNachmanBisect_Mt2_3322_Calculator::LesterNachmanBisect_Mt2_3322_Calculator() : Mt2_3322_Calculator("LesterNachmanBisect_Mt2_3322") {
  }

  // Note that this can return asymm_mt2_lester_bisect::MT2_ERROR if the event is incompatible with the MT2 hypothesis supplied.
  double LesterNachmanBisect_Mt2_3322_Calculator::mt2_3322_Sq(const LorentzTransverseVector& visibleA, // 3 d.o.f. 
						 const LorentzTransverseVector& visibleB, // 3 d.o.f.
						 const TwoVector& ptmiss,                 // 2 d.o.f.
						 const double mInvisibleA, const double mInvisibleB) // 2 d.o.f
  {

    const double mt2=LesterNachmanBisect_Mt2_3322_Calculator::mt2_3322(visibleA, visibleB, ptmiss, mInvisibleA, mInvisibleB);
    if (mt2 == asymm_mt2_lester_bisect::MT2_ERROR) {
        return mt2; // preserve the error code
    } else {
        return mt2*mt2;
    }
  }
  
  // Note that this can return asymm_mt2_lester_bisect::MT2_ERROR if the event is incompatible with the MT2 hypothesis supplied.
  double LesterNachmanBisect_Mt2_3322_Calculator::mt2_3322(const LorentzTransverseVector& visA, 
		    		       const LorentzTransverseVector& visB,
					      const TwoVector& ptmiss, 
					      const double mInvisibleA, const double mInvisibleB) {
    asymm_mt2_lester_bisect::disableCopyrightMessage(); // Perhaps there is a better place to put this.  It should not rate-limit calc speed here, though.

    double MT2 =  asymm_mt2_lester_bisect::get_mT2(
            visA.mass(), visA.px(), visA.py(),
            visB.mass(), visB.px(), visB.py(),
            ptmiss.px(), ptmiss.py(),
            mInvisibleA, mInvisibleB);

    return MT2; // Note that this can return asymm_mt2_lester_bisect::MT2_ERROR if the event is incompatible with the MT2 hypothesis supplied.
  }

} // end of Mt2 Namespace
