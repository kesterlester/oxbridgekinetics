c     First MT2 calculation library: D.Summers, 1999.
c     Referred to in https://arxiv.org/abs/hep-ph/9906349

      program tm
      implicit none
      real*8 m,mt2,dmt2mdm
      real*8 pl1(2),pl2(2),pm(2),pm1(2),pm2(2),mlsp
      real*8 m1,m2,mm1,mm2
      real*8 mt2m,a(2),b(2),eta,pt
      real*8 rn,tmp
      integer i


      mlsp=40.d0
      m=100.d0
      do i=1,1000
         pl1(1)=200.d0*(rn(1)-0.5d0)
         pl1(2)=200.d0*(rn(2)-0.5d0)
         pl2(1)=200.d0*(rn(3)-0.5d0)
         pl2(2)=200.d0*(rn(4)-0.5d0)
         m1=100.d0*rn(5)
         m2=100.d0*rn(6)
         pm(1)=-pl1(1)-pl2(1)+200.d0*(rn(5)-0.5d0)
         pm(2)=-pl1(2)-pl2(2)+200.d0*(rn(6)-0.5d0)
         tmp=mt2(pl1,pl2,pm,m1,m2,mlsp,14000.d0**2)
         write (6,*) i,dsqrt(tmp)
      enddo                     !i




      stop
      end

      function mt2(pl1t,pl2t,pmt,m1t,m2t,mlspt,s)
      implicit none
      real*8 mt2
      real*8 pl1t(2),pl2t(2),pmt(2),m1t,m2t,mlspt,s
      real*8 pl1(2),pl2(2),pm(2),m1,m2,mlsp
      common/vars/pl1,pl2,pm,m1,m2,mlsp
      real*8 m,dmt2mdm
      real*8 zeroin,mt2m,top,a,a2,bot,smin
      parameter(a=1.d-8)
      parameter(a2=1.d-4)
      real*8 gradient
      external gradient

      pl1(1)=pl1t(1)
      pl1(2)=pl1t(2)
      pl2(1)=pl2t(1)
      pl2(2)=pl2t(2)
      pm(1)=pmt(1)
      pm(2)=pmt(2)
      m1=m1t
      m2=m2t
      mlsp=mlspt

      smin=(dsqrt(pl1(1)**2+pl1(2)**2+m1**2)
      &     +dsqrt(pl2(1)**2+pl2(2)**2+m2**2)
      &     +dsqrt(pm(1)**2+pm(2)**2+4.d0*mlsp**2))**2
      &     -(pl1(1)+pl2(1)+pm(1))**2
      &     -(pl1(2)+pl2(2)+pm(2))**2

      if (s.lt.smin) then
         write(6,*) 'warning,',
         &        ' s is smaller than the minimum alowed value'
         write(6,*) 'returning minimum mt2 from the suplied parameters'
         mt2=mt2m(pl1,pl2,pm,m1,m2,mlsp,mlsp,2.d0*mlsp,dmt2mdm)
         return
      endif

      bot=max(2.d0*mlsp,a2)
      top=dsqrt(
      &     (dsqrt(s
      &             +(pl1(1)+pl2(1)+pm(1))**2
      &             +(pl1(2)+pl2(2)+pm(2))**2)
      &     -dsqrt(pl1(1)**2+pl1(2)**2+m1**2)
      &     -dsqrt(pl2(1)**2+pl2(2)**2+m2**2))**2
      &     -(pm(1)**2+pm(2)**2))


      mt2=mt2m(pl1,pl2,pm,m1,m2,mlsp,mlsp,bot,dmt2mdm)

      if (dmt2mdm.gt.0.d0) then
         return
      endif

      mt2=mt2m(pl1,pl2,pm,m1,m2,mlsp,mlsp,top,dmt2mdm)

      if (dmt2mdm.lt.0.d0) then
         return
      endif

      m=zeroin(bot,top,gradient,a2)
      mt2=mt2m(pl1,pl2,pm,m1,m2,mlsp,mlsp,m,dmt2mdm)

      return
      end

      function mt2m(pl1,pl2,pm,m1,m2,mm1,mm2,m,dmt2mdm)
      implicit none
      real*8 mt2m,dmt2mdm
      real*8 pl1(2),pl2(2),pm(2),m1,m2,mm1,mm2
      real*8 e1,e2,em,m
      real*8 d,th,th1,th2,ph,coseta
      real*8 p1p2,pmp1,pmp2,tmp
      real*8 dthdm,dphdm,dddm,dcosetadm
      real*8 dpmp1dm,dpmp2dm,demdm
      real*8 e1c,e2c,p1c,p2c
      real*8 de1cdm,de2cdm,dp1cdm,dp2cdm
      real*8 em1c,em2c,pm1c,pm2c
      real*8 dem1cdm,dem2cdm,dpm1cdm,dpm2cdm
      real*8 pi
      parameter (pi=3.14159265358979323d0)

c     these are in the frame supplied - note we change frame
c     below to the com frame for pm
c     for the com frame we append a c to variables
      e1=dsqrt(m1**2+pl1(1)**2+pl1(2)**2)
      e2=dsqrt(m2**2+pl2(1)**2+pl2(2)**2)
      em=dsqrt(m**2+pm(1)**2+pm(2)**2)

      demdm=m/em

      pmp1=em*e1-pm(1)*pl1(1)-pm(2)*pl1(2)
      pmp2=em*e2-pm(1)*pl2(1)-pm(2)*pl2(2)
      p1p2=e1*e2-pl1(1)*pl2(1)-pl1(2)*pl2(2)

      dpmp1dm=e1*demdm
      dpmp2dm=e2*demdm


c     these are the energies and magnitudes of momenta in the pm rest frame
      e1c=pmp1/m
      e2c=pmp2/m
      em1c=(m+(mm1**2-mm2**2)/m)/2.d0
      em2c=(m-(mm1**2-mm2**2)/m)/2.d0
      p1c=sqrt(e1c**2-m1**2)
      p2c=sqrt(e2c**2-m2**2)
      pm1c=sqrt(em1c**2-mm1**2)
      pm2c=sqrt(em2c**2-mm2**2)
c     derivates
      de1cdm=(m*dpmp1dm-pmp1)/m**2
      de2cdm=(m*dpmp2dm-pmp2)/m**2
      dem1cdm=0.5d0*(1.d0-(mm1**2-mm2**2)/m**2)
      dem2cdm=0.5d0*(1.d0+(mm1**2-mm2**2)/m**2)
      dp1cdm=de1cdm*e1c/p1c
      dp2cdm=de2cdm*e2c/p2c
      dpm1cdm=dem1cdm*em1c/pm1c
      dpm2cdm=dem2cdm*em2c/pm2c


c     angle between p1 and p2 in com frame
      coseta=(e1c*e2c-p1p2)/p1c/p2c
      dcosetadm=(de1cdm*e2c+e1c*de2cdm)/p1c/p2c
      & -(e1c*e2c-p1p2)*(dp1cdm*p2c+p1c*dp2cdm)/(p1c*p2c)**2




c     if the minimum of the p1 curve is above the p2 curve at that
c     point then that is the solution and visa versa
      if ((mm1**2+m1**2+2.d0*(e1c*em1c-p1c*pm1c)).gt.
      &     (mm2**2+m2**2+2.d0*(e2c*em2c+p2c*pm2c*coseta))) then
      mt2m=mm1**2+m1**2+2.d0*(e1c*em1c-p1c*pm1c)
      dmt2mdm=2.d0*(de1cdm*em1c+e1c*dem1cdm
      &                -dp1cdm*pm1c-p1c*dpm1cdm)
      return
      elseif ((mm2**2+m2**2+2.d0*(e2c*em2c-p2c*pm2c)).gt.
         &     (mm1**2+m1**2+2.d0*(e1c*em1c+p1c*pm1c*coseta))) then
         mt2m=mm2**2+m2**2+2.d0*(e2c*em2c-p2c*pm2c)
         dmt2mdm=2.d0*(de2cdm*em2c+e2c*dem2cdm
         &                -dp2cdm*pm2c-p2c*dpm2cdm)
         return
      endif

      d=2.d0*dsqrt((p1c*pm1c)**2+(p2c*pm2c)**2
      &     +2.d0*p1c*pm1c*p2c*pm2c*coseta)
      dddm=4.d0*(p1c*pm1c*(dp1cdm*pm1c+p1c*dpm1cdm)
      &     +p2c*pm2c*(dp2cdm*pm2c+p2c*dpm2cdm)
      &     +dp1cdm*pm1c*p2c*pm2c*coseta
      &     +p1c*dpm1cdm*p2c*pm2c*coseta
      &     +p1c*pm1c*dp2cdm*pm2c*coseta
      &     +p1c*pm1c*p2c*dpm2cdm*coseta
      &     +p1c*pm1c*p2c*pm2c*dcosetadm)/d


      ph=dasin(1.d0/dsqrt(1.d0+(p2c*pm2c)**2*(1.d0-coseta**2)/
      &                      (p2c*pm2c*coseta+p1c*pm1c)**2))

      dphdm=((p2c*pm2c)**2*(1.d0-coseta**2)*
      &     (dp2cdm*pm2c*coseta
      &     +p2c*dpm2cdm*coseta
      &     +p2c*pm2c*dcosetadm
      &     +dp1cdm*pm1c
      &     +p1c*dpm1cdm)/(p2c*pm2c*coseta+p1c*pm1c)**3
      &     +((p2c*pm2c)**2*coseta*dcosetadm
      & -p2c*pm2c*(dp2cdm*pm2c+p2c*dpm2cdm)*(1.d0-coseta**2))
      &      /(p2c*pm2c*coseta+p1c*pm1c)**2)
      &     *dsin(ph)**3/dcos(ph)



      th=dasin((m1**2+mm1**2-m2**2-mm2**2
      &          +2.d0*(e1c*em1c-e2c*em2c))/d)
      th1=th-ph
      th2=pi-th-ph

      if (dcos(th1).gt.dcos(th2)) then
         th=th1
      else
         th=th2
      endif

      dthdm=(2.d0*(de1cdm*em1c+e1c*dem1cdm
      &     -de2cdm*em2c-e2c*dem2cdm)/d
      &     -(m1**2+mm1**2-m2**2-mm2**2
      &          +2.d0*(e1c*em1c-e2c*em2c))/d/d*dddm)
      &     /dcos(th+ph)
      &     -dphdm



      mt2m=m1**2+mm1**2+2.d0*(e1c*em1c-p1c*pm1c*dcos(th))
      dmt2mdm=2.d0*(de1cdm*em1c+e1c*dem1cdm
      &     -dp1cdm*pm1c*dcos(th)
      &     -p1c*dpm1cdm*dcos(th)
      &     +p1c*pm1c*dsin(th)*dthdm)


      end

      function gradient(m)
      implicit none
      real*8 gradient,m,tmp,mt2m
      real*8 pl1(2),pl2(2),pm(2),m1,m2,mlsp
      common/vars/pl1,pl2,pm,m1,m2,mlsp

      tmp=mt2m(pl1,pl2,pm,m1,m2,mlsp,mlsp,m,gradient)

      return
      end

c     To get d1mach, mail netlib
c     send d1mach from core
      double precision function zeroin(ax,bx,f,tol)
      double precision ax,bx,f,tol
c     
c     a zero of the function  f(x)  is computed in the interval ax,bx .
c     
c     input..
c     
c     ax     left endpoint of initial interval
c     bx     right endpoint of initial interval
c     f      function subprogram which evaluates f(x) for any x in
c     the interval  ax,bx
c     tol    desired length of the interval of uncertainty of the
c     final result (.ge.0.)
c     
c     output..
c     
c     zeroin abscissa approximating a zero of  f  in the interval ax,bx
c     
c     it is assumed  that   f(ax)   and   f(bx)   have  opposite signs
c     this is checked, and an error message is printed if this is not
c     satisfied.   zeroin  returns a zero  x  in the given interval
c     ax,bx  to within a tolerance  4*macheps*abs(x)+tol, where macheps  is
c     the  relative machine precision defined as the smallest representable
c     number such that  1.+macheps .gt. 1.
c     this function subprogram is a slightly  modified translation  of
c     the algol 60 procedure  zero  given in  richard brent, algorithms for
c     minimization without derivatives, prentice-hall, inc. (1973).
c     
      double precision  a,b,c,d,e,eps,fa,fb,fc,tol1,xm,p,q,r,s
      double precision  dabs
c     double precision  d1mach
c     10 eps = d1mach(4)
c     set machine accuracy to zero (as I don't have it) and just use tol
 10   eps = 0.d0
      tol1 = eps+1.0d0
c     
      a=ax
      b=bx
      fa=f(a)
      fb=f(b)
c     check that f(ax) and f(bx) have different signs
      if (fa .eq.0.0d0 .or. fb .eq. 0.0d0) go to 20
      if (fa * (fb/dabs(fb)) .le. 0.0d0) go to 20
      write(6,2500)
 2500 format(1x,'f(ax) and f(bx) do not have different signs,',
 1    ' zeroin is aborting')
      stop
 20   c=a
      fc=fa
      d=b-a
      e=d
 30   if (dabs(fc).ge.dabs(fb)) go to 40
      a=b
      b=c
      c=a
      fa=fb
      fb=fc
      fc=fa
 40   tol1=2.0d0*eps*dabs(b)+0.5d0*tol
      xm = 0.5d0*(c-b)
      if ((dabs(xm).le.tol1).or.(fb.eq.0.0d0)) go to 150
c     
c     see if a bisection is forced
c     
      if ((dabs(e).ge.tol1).and.(dabs(fa).gt.dabs(fb))) go to 50
      d=xm
      e=d
      go to 110
 50   s=fb/fa
      if (a.ne.c) go to 60
c     
c     linear interpolation
c     
      p=2.0d0*xm*s
      q=1.0d0-s
      go to 70
c     
c     inverse quadratic interpolation
c     
 60   q=fa/fc
      r=fb/fc
      p=s*(2.0d0*xm*q*(q-r)-(b-a)*(r-1.0d0))
      q=(q-1.0d0)*(r-1.0d0)*(s-1.0d0)
 70   if (p.le.0.0d0) go to 80
      q=-q
      go to 90
 80   p=-p
 90   s=e
      e=d
      if (((2.0d0*p).ge.(3.0d0*xm*q-dabs(tol1*q))).or.(p.ge.
      *dabs(0.5d0*s*q))) go to 100
      d=p/q
      go to 110
 100  d=xm
      e=d
 110  a=b
      fa=fb
      if (dabs(d).le.tol1) go to 120
      b=b+d
      go to 140
 120  if (xm.le.0.0d0) go to 130
      b=b+tol1
      go to 140
 130  b=b-tol1
 140  fb=f(b)
      if ((fb*(fc/dabs(fc))).gt.0.0d0) go to 20
      go to 30
 150  zeroin=b
      return
      end

C     
      FUNCTION RN(XDUMMY)
      IMPLICIT REAL*8(A-H,O-Z)
      integer xdummy
      DIMENSION N(55)
      DATA N/
      . 980629335, 889272121, 422278310,1042669295, 531256381,
      . 335028099,  47160432, 788808135, 660624592, 793263632,
      . 998900570, 470796980, 327436767, 287473989, 119515078,
      . 575143087, 922274831,  21914605, 923291707, 753782759,
      . 254480986, 816423843, 931542684, 993691006, 343157264,
      . 272972469, 733687879, 468941742, 444207473, 896089285,
      . 629371118, 892845902, 163581912, 861580190,  85601059,
      . 899226806, 438711780, 921057966, 794646776, 417139730,
      . 343610085, 737162282,1024718389,  65196680, 954338580,
      . 642649958, 240238978, 722544540, 281483031,1024570269,
      . 602730138, 915220349, 651571385, 405259519, 145115737/
      DATA M/1073741824/
      DATA RM/0.9313225746154785D-09/
      DATA K/55/,L/31/
      IF(K.EQ.55) THEN
         K=1
      ELSE
         K=K+1
      ENDIF
      IF(L.EQ.55) THEN
         L=1
      ELSE
         L=L+1
      ENDIF
      J=N(L)-N(K)
      IF(J.LT.0) J=J+M
      N(K)=J
      RN=J*RM
      END
      
