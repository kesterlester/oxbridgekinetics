cmake_minimum_required(VERSION 2.8)
project(oxbridgekinetics)

set(oxbridgekinetics_VERSION_MAJOR 1)
set(oxbridgekinetics_VERSION_MINOR 3)
set(oxbridgekinetics_VERSION_PATCH 0)


option(DEBUG "Turn on debugging" OFF)
option(UseROOT "UseROOT" ON) # By default, use ROOT if available

message(STATUS "
============
Welcome to the Oxbridgekinetics library.

When installing, there are two main variables that you may want to use with cmake. These are: 
-DUseROOT=ON/OFF (default ON)
-DCMAKE_INSTALL_PREFIX=path/to/install/dir 
UseROOT will toggle the use of ROOT libaries ON or OFF if they are available. If ROOT is not available on your system, the compilation will fail unless you set UseROOT=OFF, the library will still be built but not with complete functionality! Within ROOT, Minuit2 is also required and if that is not available compilation will also fail unless UseROOT=OFF. CMAKE_INSTALL_PREFIX sets the path where the oxbridgekinetics library will be installed after typing 'make install'. 

A typical installation will proceed as follows:

mkdir build && cd build
cmake .. -DCMAKE_INSTALL_PREFIX=path/to/install/dir
make 
make install
=============
"
)




if(DEBUG)
  message(STATUS "debug ${DEBUG}")
  message(STATUS "use root? ${UseROOT}")
  message(STATUS "CMAKE_CURRENT_SOURCE_DIR = ${CMAKE_CURRENT_SOURCE_DIR}")
  message(STATUS "CMAKE_INSTALL_LIBDIR = ${CMAKE_INSTALL_LIBDIR}")
  message(STATUS "CMAKE_INSTALL_PREFIX = ${CMAKE_INSTALL_PREFIX}")
endif()



##################
# Add ROOT
##################

find_program(ROOT_IS_AVAILABLE NAMES root-config) 

if(ROOT_IS_AVAILABLE AND UseROOT)
  message(STATUS "A ROOT installation has been detected, and will be used.")
elseif(ROOT_IS_AVAILABLE AND NOT UseROOT)
  message(STATUS "A ROOT installation has been detected, but will not be used.")
elseif (NOT ROOT_IS_AVAILABLE AND UseROOT) 
  message(FATAL_ERROR "No root-config script was found.
  Either set up ROOT, or supply a '-DUseROOT=OFF' argument to cmake to disable the building of root-dependent tools.")
else()
  message(STATUS "ROOT installation not detected, will continue without it.")
endif()


if(UseROOT AND ROOT_IS_AVAILABLE)

  list(APPEND CMAKE_PREFIX_PATH $ENV{ROOTSYS})

  # Get ROOT version
  execute_process(COMMAND root-config --version OUTPUT_VARIABLE root_version OUTPUT_STRIP_TRAILING_WHITESPACE)
  #message(STATUS "root version: ${root_version}")

  # Extract major, minor, and patch versions from root_version
  string(REGEX REPLACE "^([0-9]+)\\.[0-9][0-9]+\\/[0-9][0-9]+.*" "\\1" ROOT_VERSION_MAJOR "${root_version}")
  string(REGEX REPLACE "^[0-9]+\\.([0-9][0-9])+\\/[0-9][0-9]+.*" "\\1" ROOT_VERSION_MINOR "${root_version}")
  string(REGEX REPLACE "^[0-9]+\\.[0-9][0-9]+\\/([0-9][0-9]+).*" "\\1" ROOT_VERSION_PATCH "${root_version}")
  string(REGEX REPLACE "/" "." ROOT_VERSION_NORM "${root_version}")
  message(STATUS "Found ROOT version ${ROOT_VERSION_NORM}")

  # find if ROOT has minuit2
  execute_process(COMMAND root-config --has-minuit2 OUTPUT_VARIABLE root_has_minuit2 OUTPUT_STRIP_TRAILING_WHITESPACE)
  message(STATUS "has root: ${root_has_minuit2}")
  if(root_has_minuit2 STREQUAL "yes")
    message(STATUS "minuit2 library detected within ROOT installation, will continue to build all libraries")
  else()
    message(FATAL_ERROR "Your ROOT installation does not contain support for the minuit2 library.  Do one of the following three options: \
    1: If you know that you only want to use kinematic variables that do NOT require the use of minuit then you can build a cut-down version of the OxbridgeKinetics library by supplying the -DUseROOT option to configure or \
    2: Find a ROOT installation that contains minuit2  which you can test with 'root-config --has-minuit2' or \
    3: Build your own ROOT installation from source remembering to give ROOT the '-Dminuit2=ON' flag to the 'cmake' command when building ROOT, before typing 'make' for ROOT.")
  endif()

  # finish loading ROOT
  find_package(ROOT REQUIRED COMPONENTS MathCore RIO Hist Tree Net Minuit2)
  #---Define useful ROOT functions and macros (e.g. ROOT_GENERATE_DICTIONARY)
  include(${ROOT_USE_FILE})
  include_directories(${CMAKE_SOURCE_DIR} ${ROOT_INCLUDE_DIRS})
  add_definitions(${ROOT_CXX_FLAGS})

endif()


######################
# Continue with compilation
######################

if(UseROOT AND ROOT_IS_AVAILABLE AND root_has_minuit2 STREQUAL "yes")
  # compile everything 
  file(GLOB headers "Mt2/*.h")
  file(GLOB sources "src/*.cpp")
else()  
  # only compile things that don't use ROOT
  set(sources 
      src/Analytic_Mt2_330_Calculator.cpp 
      src/Mt2Vectors.cpp 
      src/Mt2Calculator.cpp 
      src/mt2_bisect.cpp 
      src/LesterNachmanBisect_Mt2_3322_Calculator.cpp 
      src/ChengHanBisect_Mt2_332_Calculator.cpp 
      src/Analytic_Mt2_2220_Calculator.cpp 
      src/Mt2LorentzTransverseVector.cpp 
      src/Mt2LorentzVector.cpp 
      src/Mt2Util.cpp 
      src/Mt2TwoVector.cpp 
      src/M2C_332Calculator.cpp 
      src/MC_330_Calculator.cpp 
      src/MCT_330_Calculator.cpp 
      src/MCTll_332_Calculator.cpp 
      src/MCTT_332_Calculator.cpp
      #src/Quartic.h 
      #src/mt2_bisect.h 
      #src/lester_mt2_bisect.h 
      )
  set(headers 
      Mt2/Advanced_Mt2_332_Calculator.h 
      Mt2/AlphaT_Multijet_Calculator.h 
      Mt2/Analytic_Mt2_2220_Calculator.h 
      Mt2/Analytic_Mt2_330_Calculator.h 
      Mt2/Analytic_Mt2_332_Assistor.h 
      Mt2/Basic_M2C_332_Calculator.h 
      Mt2/Basic_MPairProd_Calculator.h 
      Mt2/Basic_MtGen_330_Calculator.h 
      Mt2/Basic_MtGen_332_Calculator.h 
      Mt2/LesterNachmanBisect_Mt2_3322_Calculator.h 
      Mt2/LesterNachmanBisect_Mt2_332_Calculator.h 
      Mt2/ChengHanBisect_Mt2_332_Calculator.h 
      Mt2/Frugal_MPairProd_Calculator.h 
      Mt2/Frugal_MtGen_330_Calculator.h
      Mt2/Frugal_MtGen_332_Calculator.h 
      Mt2/MC_330_Calculator.h 
      Mt2/MCT2_332_Calculator.h 
      Mt2/MCT_330_Calculator.h 
      Mt2/MCTll_332_Calculator.h 
      Mt2/MCTT_332_Calculator.h 
      Mt2/MPairProd_Calculator.h 
      Mt2/Mt2_300_Calculator.h 
      Mt2/Mt2_302_Calculator.h 
      Mt2/Mt2_330_Calculator.h 
      Mt2/Mt2_332_Calculator.h 
      Mt2/Mt2_3322_Calculator.h 
      Mt2/Mt2ApproximatingAdapter_332_from_330.h 
      Mt2/Mt2_AsymmParents332_Calculator.h 
      Mt2/Mt2Calculator.h 
      Mt2/Mt2Calculators.h 
      Mt2/Mt2LorentzTransverseVector.h 
      Mt2/Mt2LorentzVector.h 
      Mt2/Mt2MinimiserGsl.h 
      Mt2/Mt2Minimiser.h 
      Mt2/Mt2MinimiserNag.h 
      Mt2/Mt2TwoVector.h 
      Mt2/Mt2Units.h 
      Mt2/Mt2Util.h 
      Mt2/Mt2Vectors.h 
      Mt2/MtGen_330_Calculator.h 
      Mt2/MtGen_332_Calculator.h 
      Mt2/Nt2_332_Calculator.h 
      Mt2/SolutionType.h
  )
endif()

if(DEBUG)
  message(STATUS "headers: ${headers}")
  message(STATUS "sources: ${sources}")
  message(STATUS "root libs: ${ROOT_LIBRARIES}")
endif()



######################
# Create the library
######################

add_library(
  oxbridgekinetics STATIC
  ${sources}
  ${headers}
  ${ROOT_LIBRARIES} # this is empty if there is no ROOT
  )

# set the header files that will be copied into the "install" directory (public header files)
set_target_properties(oxbridgekinetics PROPERTIES PUBLIC_HEADER "${headers}")

target_include_directories(oxbridgekinetics PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}")
#target_include_directories(oxbridgekinetics 
#    PUBLIC
#        $<INSTALL_INTERFACE:Mt2>
#        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/Mt2>
#    PRIVATE
#        ${CMAKE_CURRENT_SOURCE_DIR}/src
#        )
                          
                          

###########################
# Compile examples
###########################

add_subdirectory(examples)

###########################
# add installation for library 
###########################


install(TARGETS oxbridgekinetics 
        LIBRARY DESTINATION ${CMAKE_INSTALL_PREFIX}
        ARCHIVE DESTINATION ${CMAKE_INSTALL_PREFIX}
        PUBLIC_HEADER DESTINATION include/Mt2)
