// This new code should still be compatible with Analytic_Mt2-2220_calculator. However because I want to be able to find the right root
// and calculate Mt2, I now start with the basic azimuthal coordinates of a, b and ptmiss.
// The "engine" section should still be pastable into Analytic_Mt2-2220_calculator with minimal adjustments,
// However the "engine" now covers both calculating the roots, finding the right root and finding the smallest Mt2 value.
// Of course it can now obviously be run as a standalone piece of code.
#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    std::cout.precision(12);

// First part just sets-up all the same named variables as found in the Analytic_Mt2-2220_calculator
// Now it is getting the x and y components of a, b and ptmiss from a datafile as its starting point.
// Obviously some of the first few lines of code are stolen completely from the Analytic_Mt2-2220_calculator code!

    double inputaX;
    double inputaY;
    double inputbX;
    double inputbY;
    double inputptmissX;
    double inputptmissY;

    int iloop = 1;

    while (std::cin>>inputaX) {
        std::cin>>inputaY>>inputbX>>inputbY>>inputptmissX>>inputptmissY;

    const double a = sqrt((inputaX*inputaX)+(inputaY*inputaY));
    const double b = sqrt((inputbX*inputbX)+(inputbY*inputbY));

    const double adotp = (inputaX*inputptmissX + inputaY*inputptmissY);
    const double bdotp = (inputbX*inputptmissX + inputbY*inputptmissY);
    const double ahdotbh = cos(atan2(inputaY,inputaX)-atan2(inputbY,inputbX));

    const double eap = (-inputaX*inputptmissY + inputaY*inputptmissX);
    const double ebp = (-inputbX*inputptmissY + inputbY*inputptmissX);
    const double eahbh = sin(atan2(inputaY,inputaX)-atan2(inputbY,inputbX));

// For the special case of ma=0, mb=0, chi=0, MT2 is *zero* whenever the ptmiss vector is "between" visA and visB, since this always admits a solution in which p and q are parallel to a and b (respectively) making MT_a = MT_b = 0.  So find coeffs of ptmiss in the a,b basis, and see if they are both positive.  If they are, return zero:
    
    double MT2 = 0.0;

    if (!(eap/eahbh >= 0 &&  -ebp/eahbh >= 0)) {

     const double deltadotp = adotp - bdotp;
     const double sigmadotp = adotp + bdotp;
     const double esigmap = eap + ebp;
     const double edeltap = eap - ebp;

     const double Kss = - deltadotp * eahbh;
     const double Kcc =  - esigmap * ahdotbh;
     const double Ks = sigmadotp;
     const double Kc = esigmap;
     const double Kcs = -sigmadotp * ahdotbh - edeltap * eahbh;
     const double K1 = 0.0;

// For a polynomial where Kss is the coeff of sin^2 and Ksc is the coefficent of the sin cos term, etc, then if you substitute sin=sqrt(1-cos^2) and then rearrange for a polynomial exclusively in cos, you get a polynomial with the following coeffs ... see 20100622a-mt2-masslessinvisiblesandmasslesschi-lookingfornicetriganswers-dumpdotprods-1.nb// For Sine Coeffs
     const double coeffSin0 = (K1 - Kc + Kcc)*(K1 + Kc + Kcc);
     const double coeffSin1 = 2.*(-Kcs* Kc + Ks*(K1 + Kcc));
     const double coeffSin2 = (Ks*Ks) - (Kcs*Kcs) + (Kc*Kc) + 2.*(Kss - Kcc)*(K1 + Kcc);
     const double coeffSin3 = 2.*(Kcs*Kc + Ks*(Kss - Kcc));
     const double coeffSin4 = (Kcs*Kcs) + ((Kss - Kcc)*(Kss-Kcc));

// NB we could get the same for coeffs of sin by interchanging s with c (and noting that Ksc should be re-written  as Kcs).

     const double AA=(coeffSin4 / coeffSin4);
     const double BB=(coeffSin3 / coeffSin4);
     const double CC=(coeffSin2 / coeffSin4);
     const double DD=(coeffSin1 / coeffSin4);
     const double EE=(coeffSin0 / coeffSin4);


// These next two variables will eventually hold the real physical root that gives the smallest value for Mt2
    double physicalSinTheta = 0.0;
    double physicalCosTheta = 0.0;

// As quartic solvers tend to be unstable when the real root is zero, we need to identify this situation first.
// It is simply when the EE coefficient (ie that multiplying x^0) is zero (then by definition x=0 must be a root).
// So let's check for EE=0, if it is we know the root we want is sin theta = 0 and we can go straight to calculating Mt2.
// (Note this is a reasonably common occurance in low UTM scenarios as the theta is always 180 degrees - see paper!)
    if (fabs(EE) - 0.000000000000000000001 > 0) {

//START OF ENGINE:
//Need to define the following functions based on the quartic coefficients - these and the rest of the method are as per
//Nickalls (2009) describing Euler's method

    const double Pi = acosl(-1.0L);
    const double J_var = 72*AA*CC*EE + 9*BB*CC*DD - 27*AA*(DD*DD) - 27*EE*(BB*BB) - 2*(CC*CC*CC);
    const double I_var = 12*AA*EE - 3*BB*DD + (CC*CC);
    const double X_Nf = -BB / (4*AA);
    const double Y_Nf = ((BB*BB*BB) + 8*(AA*AA)*DD - 4*AA*BB*CC)/(8*(AA*AA));
    const double epsilon_squared = (3*(BB*BB) - 8*AA*CC)/(48*(AA*AA));

//Next we check to see if we have two or four real roots
//If (J_var^2 - 4*I_var^3)>0 we have two real roots otherwise four real roots
//(unless inputs are non-physical and for the latter we have four complex roots!)

    bool Four_Real_Roots = false;
    bool Two_Real_Roots = false;
    bool No_Real_Roots = false;
    bool No_Roots_At_All = false;

    double long discriminant = 4*(I_var*I_var*I_var) - (J_var*J_var);

    if (discriminant < 0.0)
    {
        Two_Real_Roots = true;
    }
    else if (discriminant >= 0.0)
    {
        Four_Real_Roots = true;
    }
    else
    {
        No_Roots_At_All = true;
    }
//There are two different methods depending on all Real or some Complex roots.
//So engine now splits into two sections depending on expected roots.
//Both methods however are similar in that they first of all find the roots of
//a resolvent cubic (alpha, beta and gamma), and the square root of these leads
//to the quartic roots:

    double alpha_var = 0;
    double beta_var = 0;
    double gamma_var = 0;

    double euler_r1_real = 0;
    double euler_r2_real = 0;
    double euler_r3_real = 0;

    double squareroot_r1_real = 0;
    double squareroot_r2_real = 0;
    double squareroot_r3_real = 0;

//Method 1: four real roots (and three real resolvent cubic roots):
    if (Four_Real_Roots) {
        double euler_theta;
        double cosine_3_euler_theta;

        cosine_3_euler_theta = - J_var/sqrt(4*(I_var*I_var*I_var));
        if (cosine_3_euler_theta > 1 || discriminant == 0) {
            cosine_3_euler_theta = 1.0;
            euler_theta = 0.0;
        }
        else {
            euler_theta = acos(cosine_3_euler_theta)/3;
        }

        alpha_var = 2*sqrt(I_var)*cos(euler_theta);
        beta_var = 2*sqrt(I_var)*cos(2*Pi/3 + euler_theta);
        gamma_var = 2*sqrt(I_var)*cos(4*Pi/3 + euler_theta);

//Next we use these roots to find three "Euler roots" which are then used to find
//the quartic roots (by adding permutations of the square roots of the "Euler roots"!)

        euler_r1_real = epsilon_squared + (alpha_var/(12*AA));
        euler_r2_real = epsilon_squared + (beta_var/(12*AA));
        euler_r3_real = epsilon_squared + (gamma_var/(12*AA));

//before we go any further, must check we are not heading towards four complex roots!!
//(this would be where first of the above euler roots are negative! Note, rounding may cause r2 and r3 to seem negative, but in this case these are really zero!)

        if (euler_r1_real<0) {
            No_Real_Roots = true;
        }
        if (euler_r2_real<0 || euler_r3_real<0) {
            euler_r2_real = 0.0;
            euler_r3_real = 0.0;
        }
        if (~No_Real_Roots) {
            squareroot_r1_real = sqrt(euler_r1_real);
            squareroot_r2_real = sqrt(euler_r2_real);
            squareroot_r3_real = sqrt(euler_r3_real);
        }
    }
//Method 2 (ie where we have two real roots and two complex roots)
//Note we must use a different method to calculate the two real roots (as the
//resolvent cubic will have two complex roots and one real root!)
    if (Two_Real_Roots) {

        double beta_var_imaginary = 0.0;
        double euler_r2_imaginary = 0.0;

        double firstThingToCubeRoot = 0.5*(-J_var + sqrt(-discriminant));
        double secondThingToCubeRoot = 0.5*(-J_var - sqrt(-discriminant));

        double firstCubeRoot = firstThingToCubeRoot/fabs(firstThingToCubeRoot)*pow(fabs(firstThingToCubeRoot),1./3.);
        double secondCubeRoot = secondThingToCubeRoot/fabs(secondThingToCubeRoot)*pow(fabs(secondThingToCubeRoot),1./3.);

        alpha_var = firstCubeRoot + secondCubeRoot;
        beta_var = -alpha_var/2;
        beta_var_imaginary = (sqrt(3)/2)*sqrt((alpha_var*alpha_var)-4*I_var);

        euler_r1_real = epsilon_squared + (alpha_var/(12*AA));
        euler_r2_real = epsilon_squared + (beta_var/(12*AA));
        euler_r2_imaginary =  (beta_var_imaginary/(12*AA));

        squareroot_r1_real = sqrt(euler_r1_real);
        squareroot_r2_real = (1/sqrt(2))*sqrt(sqrt((euler_r2_real*euler_r2_real) + (euler_r2_imaginary*euler_r2_imaginary)) + euler_r2_real);
        squareroot_r3_real = squareroot_r2_real;
    }
//OK we now have the three Euler square roots of the resolvent cubic roots.
//Now we just have to combine them to get the quartic roots - NOTE There is no longer any complex numbers involved!

//First thing we need to do is check if euler numbers combine to give a "negative" or "positive" value
//We do this by looking at Y_Nf: if >0, signs of roots combine to give four "negative" permutations: ++-, +-+, -++, ---
//If Y_Nf < 0, we need the four "positive" combinations of signs: -+-, --+, +--, +++

    double goodSinValues_1 = 0;
    double goodSinValues_2 = 0;
    double goodSinValues_3 = 0;
    double goodSinValues_4 = 0;

    int rootNumber = 0;
    if(No_Real_Roots) {
//        std::cout<< "No Real Roots Found!"<<std::endl;
    }
    else
    {
    if (Y_Nf > 0) {
        if (discriminant < 0) {
            rootNumber = 2;
	    goodSinValues_1 = (X_Nf - squareroot_r1_real + squareroot_r2_real + squareroot_r3_real);
	    goodSinValues_2 = (X_Nf - squareroot_r1_real - squareroot_r2_real - squareroot_r3_real);
        }
        else
        {
            rootNumber = 4;
            goodSinValues_1 = (X_Nf - squareroot_r1_real + squareroot_r2_real + squareroot_r3_real);
            goodSinValues_2 = (X_Nf - squareroot_r1_real - squareroot_r2_real - squareroot_r3_real);
            goodSinValues_3 = (X_Nf + squareroot_r1_real + squareroot_r2_real - squareroot_r3_real);
            goodSinValues_4 = (X_Nf + squareroot_r1_real - squareroot_r2_real + squareroot_r3_real);
        }
    }
    else {
        if (discriminant < 0) {
            rootNumber = 2;
	    goodSinValues_1 = (X_Nf + squareroot_r1_real - squareroot_r2_real - squareroot_r3_real);
	    goodSinValues_2 = (X_Nf + squareroot_r1_real + squareroot_r2_real + squareroot_r3_real);
        }
        else
        {
            rootNumber = 4;
            goodSinValues_1 = (X_Nf + squareroot_r1_real - squareroot_r2_real - squareroot_r3_real);
            goodSinValues_2 = (X_Nf + squareroot_r1_real + squareroot_r2_real + squareroot_r3_real);
            goodSinValues_3 = (X_Nf - squareroot_r1_real + squareroot_r2_real - squareroot_r3_real);
            goodSinValues_4 = (X_Nf - squareroot_r1_real - squareroot_r2_real + squareroot_r3_real);
        }
    }
    }

//OK we've finished getting the roots and note the goodSinValues roots are now always 2 or 4 Real roots -
//so technically the Mt2 program doesn't need to worry about complex values from now on......

//Now to find the physical root (or if more than 1, the one that gives the lowest value of Mt2)

//In most cases there will only be one physical root - the other one or three roots will correspond to negative values of |p| and |q|
//The Physical Root is found by realising that, as a non-zero Mt2 value must have ptmiss "outside" of a and b, and as p and q must
//have ptmiss "inside" them, then theta must be an angle that rotates p and q through a positive angle not less than the minimum of
//the angles between a and ptmiss and b and ptmiss, and an angle not greater than the maximum of the angles between a and ptmiss
//and b and ptmiss.

//First thing we need to do is calculate the positive angles between a and ptmiss and b and ptmiss

    double aPositiveAnglePtmiss = 0.0;
    double bPositiveAnglePtmiss = 0.0;
    const double aAnglePtmiss = atan2((inputaX*inputptmissY-inputaY*inputptmissX),(inputaX*inputptmissX + inputaY*inputptmissY));
    const double bAnglePtmiss = atan2((inputbX*inputptmissY-inputbY*inputptmissX),(inputbX*inputptmissX + inputbY*inputptmissY));

    if (aAnglePtmiss <0) {
        aPositiveAnglePtmiss = (2*Pi) + aAnglePtmiss;
    } else {
        aPositiveAnglePtmiss = aAnglePtmiss;
    }
    if (bAnglePtmiss <0) {
        bPositiveAnglePtmiss = (2*Pi) + bAnglePtmiss;
    } else {
        bPositiveAnglePtmiss = bAnglePtmiss;
    }
    const double minAngle = min(aPositiveAnglePtmiss , bPositiveAnglePtmiss);
    const double maxAngle = max(aPositiveAnglePtmiss , bPositiveAnglePtmiss);

//Next we need to do is calculate the angle theta from the roots (and for this we need to calculate the cos(theta) values) and then we compare to these max and min angles to find the physical root

	int rootFlag = 0;
        int rootFlag_1 = 0;
        int rootFlag_2 = 0;
        int rootFlag_3 = 0;
        int rootFlag_4 = 0;

//So first we find cos(theta) - similar to the previous code, but a little bit more straightforward in that we don't compare "LHS" and "RHS" to find the sign, we just divide one by the other and a correctly signed cos(theta) is the answer!
        const double s_1 = goodSinValues_1;
        double currentTheta = 0.0;
	double cosTheta = 0.0;
        double LHS = K1 + Kcc + s_1*(Ks + (-Kcc + Kss)*s_1);
        double RHS = -(Kc + Kcs*s_1);
        double ratiovalue = LHS / RHS;
        if (ratiovalue < -1.) { ratiovalue = -1.;}
            cosTheta = ratiovalue;

        if (cosTheta < 0) {
            currentTheta = (Pi - asin(s_1));
        }
        else if (s_1 > 0) {
            currentTheta = (asin(s_1));
        }
        else {
            currentTheta = ((2*Pi) + asin(s_1));
        }
        const double theta_1 = (currentTheta);

//Finally we compare theta to the positive angle between a and ptmiss and b and ptmiss
	if (currentTheta >= minAngle) {
	   if(currentTheta <= maxAngle) {
		rootFlag = rootFlag +1;
            	rootFlag_1 = 1;
           }
	}

//and repeat for the other roots (should be at least one more, but can be three more!)
        const double s_2 = goodSinValues_2;
        currentTheta = 0.0;
	cosTheta = 0.0;
        LHS = K1 + Kcc + s_2*(Ks + (-Kcc + Kss)*s_2);
        RHS = -(Kc + Kcs*s_2);
        ratiovalue = LHS / RHS;

        if (ratiovalue < -1.) { ratiovalue = -1.;}
            cosTheta = ratiovalue;

        if (cosTheta < 0) {
            currentTheta = (Pi - asin(s_2));
        }
        else if (s_2 > 0) {
            currentTheta = (asin(s_2));
        }
        else {
            currentTheta = ((2*Pi) + asin(s_2));
        }
        const double theta_2 = (currentTheta);

	if (currentTheta >= minAngle) {
	   if(currentTheta <= maxAngle) {
            	rootFlag_2 = 1;
            	rootFlag = rootFlag+1;
           }
	}

	double theta_3 = 0;
	double theta_4 = 0;
	if (rootNumber == 4) {
           const double s_3 = goodSinValues_3;
           double currentTheta = 0.0;
	   double cosTheta = 0.0;
           double LHS = K1 + Kcc + s_3*(Ks + (-Kcc + Kss)*s_3);
           double RHS = -(Kc + Kcs*s_3);
           double ratiovalue = LHS / RHS;
           if (ratiovalue < -1.) { ratiovalue = -1.;}
	   cosTheta = ratiovalue;

           if (cosTheta < 0) {
		currentTheta = (Pi - asin(s_3));
           }
           else if (s_3 > 0) {
            	currentTheta = (asin(s_3));
           }
           else {
           	 currentTheta = ((2*Pi) + asin(s_3));
           }
           theta_3 = (currentTheta);

	   if (currentTheta >= minAngle) {
	   	if(currentTheta <= maxAngle) {
            	   rootFlag_3 = 1;
            	   rootFlag = rootFlag+1;
           	}
	   }

           const double s_4 = goodSinValues_4;
           currentTheta = 0.0;
	   cosTheta = 0.0;
           LHS = K1 + Kcc + s_4*(Ks + (-Kcc + Kss)*s_4);
           RHS = -(Kc + Kcs*s_4);
           ratiovalue = LHS / RHS;
           if (ratiovalue < -1.) { ratiovalue = -1.;}
           cosTheta = ratiovalue;

           if (cosTheta < 0) {
        	currentTheta = (Pi - asin(s_4));
           }
           else if (s_4 > 0) {
        	currentTheta = (asin(s_4));
           }
           else {
		currentTheta = ((2*Pi) + asin(s_4));
           }
           theta_4 = (currentTheta);

	   if (currentTheta >= minAngle) {
		if(currentTheta <= maxAngle) {
            	   rootFlag_4 = 1;
            	   rootFlag = rootFlag+1;
		}
	   }
	}

//OK we have now identified 1 or 3 physical roots. If we have more than 1, we need to filter out the one that will give the lowest Mt2 value.
//This will be the one with theta closest to 180 degrees ie this will minimise both sides of equation 3.7 irrespective of the values of |p| and |q|

    if (rootFlag == 3) {

	double firstAngle = 0;
	double secondAngle = 0;
	double thirdAngle = 0;

	if (rootFlag_1 == 1) {
        	firstAngle = theta_1;
        	if (rootFlag_2 == 1) {
		    secondAngle = theta_2;
        	    if (rootFlag_3 == 1) {
		    	thirdAngle = theta_3;
		    } else {thirdAngle = theta_4;}
		} else {
		    secondAngle = theta_3;
		    thirdAngle = theta_4;
		}
	}
	else if (rootFlag_2 == 1) {
        	firstAngle = theta_2;
        	secondAngle = theta_3;
        	thirdAngle = theta_4;
	}

	double realTheta = 0.0;
        if (abs(Pi - firstAngle) < abs(Pi - secondAngle)) {
		if (abs(Pi - firstAngle) < abs(Pi - thirdAngle)) {
                	realTheta = firstAngle;
		} else {
			realTheta = thirdAngle;
		}
        }
        else {
                if (abs(Pi - secondAngle) < abs(Pi - thirdAngle)) {
                    realTheta = secondAngle;
                }
                else {
                    realTheta = thirdAngle;
                }
            }

        physicalSinTheta = sin(realTheta);
        physicalCosTheta = cos(realTheta);
    }
    else if (rootFlag > 0) {
// If we only have one physical root (vast majority of the time) then we are already there:
	if (rootFlag_1 == 1) {
	        physicalSinTheta = sin(theta_1);
	        physicalCosTheta = cos(theta_1);
        } else if (rootFlag_2 == 1) {
	        physicalSinTheta = sin(theta_2);
	        physicalCosTheta = cos(theta_2);
        } else if (rootFlag_3 == 1) {
	        physicalSinTheta = sin(theta_3);
	        physicalCosTheta = cos(theta_3);
        } else if (rootFlag_4 == 1) {
	        physicalSinTheta = sin(theta_4);
	        physicalCosTheta = cos(theta_4);
	}
    }
    }
    else {
// If we identified that x=0 was a root at the very start, then we end up here straightaway
        physicalSinTheta = 0;
        physicalCosTheta = -1;
    }
//OK we now have the physical root giving lowest Mt2 value so let's calculate it!
//I'm going to take a very slightly different path here to the Analytic_Mt2-2220_calculator and the paper
//    cout<<"sin(theta) = "<<physicalSinTheta<<endl;
    double MT2SQ1 = 0.0;
    double MT2SQ2 = 0.0;
    double p_over_q = 0.0;
    double thingTwo = 0.0;

    p_over_q = -
         (( physicalCosTheta * eap + physicalSinTheta * adotp) * b )/
         (( physicalCosTheta * ebp + physicalSinTheta * bdotp) * a );

    thingTwo = 1 / (physicalCosTheta + p_over_q * (physicalCosTheta * ahdotbh + physicalSinTheta * eahbh));

    const double q = thingTwo * adotp / a;
    const double p = p_over_q * q;

    MT2SQ1 = 2.0 * a * p * (1 - physicalCosTheta * ahdotbh - physicalSinTheta * eahbh);
    MT2SQ2 = 2.0 * b * q * (1 - physicalCosTheta * ahdotbh + physicalSinTheta * eahbh);

    const double meanMT2SQ = (MT2SQ1 + MT2SQ2)*0.5;

    MT2 = sqrt(meanMT2SQ);
    }

//END OF THE "EXTENDED" ENGINE!!

    cout<<" Run "<<iloop<<" Mt2 value is: "<<MT2<<endl;

    iloop = iloop + 1;
    }

    return 0;
}

