%CERTIFY%
<!-- This is the default ATLAS template. 
Please modify it in the sections indicated to create your topic! In particular, notice that at the bottom there are some sections that must be filled for publicly accessible pages.
If you have any comments/complaints about this template, then please email :
 Stephen Haywood (Computing Documentation Coordinator)
S.Haywood@rl.ac.uk
 (or failing that, edward.moyse at cern.ch)
--> 

<!-- By default the title is the WikiWord used to create this topic !-->
<!-- if you want to modify it to something more meaningful, just replace %TOPIC% below with i.e "My Topic"!-->
---+!! The Stransverse Mass Library of the Susy Group     
%TOC%
%STARTINCLUDE%

---+ Introduction  and References
<!-- Add an introduction here, describing the purpose of this topic. !--> 

OldStransverseMassLibraryImplementation

The "Cambridge MT2 Variable", sometimes also called the "Stransverse Mass", is an event variable used to bound the masses of an unseen pair of particles which are presumed to have decayed semi-invisibly into particles which were seen.  MT2 is therefore a function of the momenta of two visible particles and the missing transverse momentum in an event.   There is an external homepage for matters relating to MT2 [[http://www.hep.ucl.ac.uk/~barr/mt2/][here]] and the main references for the definition of MT2 are:

   * Chris Lester, David Summers [[http://www.slac.stanford.edu/spires/find/hep/www?key=4093909][Phys.Lett.B463:99-103,1999 ]]
   * Alan Barr, Christopher Lester, Phil Stephens [[http://www.slac.stanford.edu/spires/find/hep/www?key=5533279][J.Phys.G29:2343-2363,2003]]


Other papers with interesting contributions to understanding MT2 include:

   * [[http://arxiv.org/abs/0708.1028][arxiv.org/0708.1028 Lester and Barr: "MTGEN: Mass scale measurements in pair-production at colliders."]]
   * [[http://arxiv.org/abs/0709.0288][arxiv.org/0709.0288 Gluino Stransverse Mass. Won Sang Cho, Kiwoon Choi (KAIST, Taejon) , Yeong Gyun Kim (KAIST, Taejon & Sejong U.) , Chan Beom Park (KAIST, Taejon) . Sep 2007. 5pp. e-Print: arXiv:0709.0288]]
   * [[http://arxiv.org/abs/0709.2740][arxiv.org/0709.2740 Transverse Observables and Mass Determination at Hadron Colliders. Ben Gripaios]]

A small c++ library has been created to house algorithms that can calculate MT2.  The purposes of this library are:
   1 to remove duplication and wasted effort from ATLAS code,
   1 to make sure that the algorithms used to calculate MT2 are of high quality.
      1 (Simple implementations of MT2 can sometimes demonstrate poor performance in certain input regimes - even if they appear to work well on a few test cases.  As this is not often apparent at first sight, it is hoped that central MT algorithms might be better maintained and/or have their bugs catalogued more effectively)<br />
   1 to provide a central repository for more "specialised" forms of MT2 beyond the simple "332 type" ([[https://twiki.cern.ch/twiki/bin/view/AtlasProtected/StransverseMassLibrary#Nomenclature][see below]]) described in  [[http://www.slac.stanford.edu/spires/find/hep/www?key=4093909][Phys.Lett.B463:99-103,1999 ]]. <br />
      1 For example, other forms (so called "4441 type" -- [[https://twiki.cern.ch/twiki/bin/view/AtlasProtected/StransverseMassLibrary#Nomenclature][see below]]) take additional constraints from knowledge of the maximum centre-of-mass energy of the collision, and use information coming from the Z-momentum of the visible particles in the event.

---+ Where is the Code for the Library?   

The code is maintained in an svn repository located at:
svn://pcfs.hep.phy.cam.ac.uk/TRUNK/NonAthenaVersion/mT2

---+ StransverseMassUtils Athena interface package

To address the issue of using the libraries with the Athena framework, the [[https://svnweb.cern.ch/trac/atlasoff/browser/PhysicsAnalysis/AnalysisCommon/StransverseMassUtils][StransverseMassUtils]] package was written.  *It is documented further [[http://cdsweb.cern.ch/record/1240255?ln=en][here]] in CDS.*

The code from the above repository has been built for a variety of computer architectures in the atlas external offline software area
/afs/cern.ch/atlas/offline/external/mT2

The glue package [[https://svnweb.cern.ch/trac/atlasoff/browser/External/AtlasmT2][AtlasmT2]] is responsible for setting the cmt environment to link to the external Atlas mT2 library.  The current tag for this package is =AtlasmT2-00-00-02=.  It can be checked out
<verbatim>
cmt co -r AtlasmT2-00-00-02 External/AtlasmT2
</verbatim>

Until this package is shipped with a release, it will be necessary to check this package out and ensure that it sets up the path pointing to the external mT2 library properly.

The [[https://svnweb.cern.ch/trac/atlasoff/browser/PhysicsAnalysis/AnalysisCommon/StransverseMassUtils][StransverseMassUtils]] package does interfacing between the external mT2 library and the Athena framework.  The latest tag for this package is =StransverseMassUtils-00-00-04=.  It can be checked out
<verbatim>
cmt co -r StransverseMassUtils-00-00-04 PhysicsAnalysis/AnalysisCommon/StransverseMassUtils
</verbatim>

The Tools:
<verbatim>
mT2_332_CalcTool
mT2_330_CalcTool
mTGen_332_CalcTool
mTGen_330_CalcTool
mCTCalcTool
</verbatim>

These tools calculate the variables one expects from the name (ie =mT2_332_CalcTool= uses Basic_Mt2_332_Calculator).  These algorithms are described in more detail below.  Some of these tools give the user the option to select the calculation method.  The available options are listed in the =StransverseMassUtils/mT2Defs.h= header file.
<verbatim>
namespace Mt2{
  enum Mt2_332_CalculatorType{
    Basic332=0,
    Bisect
  };
  enum Mt2_330_CalculatorType{
    Analytic330=0
  };
  typedef I4Momentum * Atl4Vec;
  typedef const MissingET * AtlMET;
}
</verbatim>


----+++ A quick example

The tools in the package are =AlgTool='s which can be used by =Algorithm='s.  This example uses the =UserAnalysis/AnalysisSkeleton= algorithm.  First are the changes which need to be made to the =AnalysisSkeleton.h= header file.

<verbatim>
#include "StransverseMassUtils/mT2Defs.h"
#include "StransverseMassUtils/mT2CalcTool.h"
</verbatim>
  
<verbatim>
ToolHandle<mT2CalcTool> m_mT2tool;
</verbatim>

Now for the changes which need to be made to =UserAnalysis/AnalysisSkeleton.cxx= source file.

<verbatim>
#include "StransverseMassUtils/mT2Defs.h"
</verbatim>

In the class initialization list:
<verbatim>
AnalysisSkeleton::AnalysisSkeleton(const std::string& name,
ISvcLocator* pSvcLocator) : CBNT_AthenaAwareBase(name, pSvcLocator),
m_analysisTools( "AnalysisTools", this ),
m_analysisSelectionTool( "UserAnalysisSelectionTool", this ),
m_analysisPreparationTool( "UserAnalysisPreparationTool", this ),
m_analysisOverlapCheckingTool( "UserAnalysisOverlapCheckingTool", this ),
m_analysisOverlapRemovalTool( "UserAnalysisOverlapRemovalTool", this ),
m_trigDec("Trig::TrigDecisionTool") ,
m_mT2tool("mT2CalcTool",this)
{
</verbatim>

In the =CBNT_initialize()= function:
<verbatim>
StatusCode sc = m_mT2tool.retrieve();
if ( sc.isFailure() ) {
  mLog << MSG::ERROR << "Can’t get handle on mT2CalcTool" << endreq;
  return sc;
}
m_mT2tool->setType( Mt2::Bisect );
m_mT2tool->setChi( 0.0 );
</verbatim>

In the =CBNT_execute()= function the following analysis code will calculate mT2
<verbatim>
const MissingET * m_pMissing;
const JetCollection * m_jetTES;

StatusCode sc = m_storeGate->retrieve(m_jetTES,"AntiKt4H1TopoJets");
sc = m_storeGate->retrieve(m_pMissing,"MET_RefFinal");

m_mT2tool->clear();

JetCollection::const_iterator itr2 = m_jetTES->begin();
JetCollection::const_iterator itr1 = itr2++;
if ( m_jetTES->size() > 1 ){
  m_mT2tool->setMom(*itr1,*itr2,m_pMissing);
  m_mT2tool->calc(m_aan_mt2);
}
</verbatim>

The value =m_aan_mt2= is passed by reference to the calculator.  





---+ Do I need to build the library?  Can I use it with ROOT?

The answer to both questions is "yes".

There are two different ways in which the library can be used.  If you prefer to work within the ROOT framework, then see the "Using with ROOT" section below.  Alternatively, if you prefer to work outside of the ROOT framework, then you will need to build the library and link your own executables against it, or adapt one of the many example programs.

Some users *might* be able to make use of the version of the library which has been pre-compiled and attached to this Wiki page -- see the table of attached files at the bottom.  It was compiled in June 2007 for use on =LXPLUS=.  At that time, =LXPLUS= was running =slc4_amd64_gcc34= and the library was linked against =ROOT= version =5.15.08=.  

---+ To check out and build the library:<br />    
You should be able to check out the code with something like this:
<verbatim>
mkdir -p ~/some/working/directory
cd ~/some/working/directory
</verbatim>
Then to actually check out the mT2 package you need to do the following:
<verbatim>
svn co svn://pcfs.hep.phy.cam.ac.uk/TRUNK/NonAthenaVersion/mT2
</verbatim>
To retrieve the revision consistent with the Atlas external build one must specify the revision: 1346
<verbatim>
svn co -r 1346 svn://pcfs.hep.phy.cam.ac.uk/TRUNK/NonAthenaVersion/mT2
</verbatim>

Having done the above, you should then see that you have created a directory "mT2" containing some other subdirectories:
<verbatim>
[localhost] /usera/lester/some/working/directory > ls
mT2
[localhost] /usera/lester/some/working/directory > ls mT2/
bin        CVS   examples  Makefile.include  README
ChangeLog  docs  Makefile  Mt2               src
</verbatim>
Enter the main directory:
<verbatim>
cd mT2
</verbatim>
Before you can build, if you are not running on LXPLUS you will need to make sure that the file =[[http://isscvs.cern.ch/cgi-bin/cvsweb.cgi/groups/SUSY/mT2/Makefile.include?cvsroot=atlas][Makefile.include]]= is customised to reflect your own location, as this file specifies whether the external package MINUIT2 (on which the library depends) will be found.   MINUIT2 libraries come with root (or SEAL if you want them without ROOT) for example at =/afs/cern.ch/atlas/software/releases/sw/lcg/external/root/5.13.04b/slc3_ia32_gcc323/root/lib/libMinuit2.so=.
The only lines of the file =[[http://isscvs.cern.ch/cgi-bin/cvsweb.cgi/groups/SUSY/mT2/Makefile.include?cvsroot=atlas][Makefile.include]]= which you might need to edit are the following: (the default values of =slc4_amd64_gcc34= work on LXPLUS as of 7th Nov 2008 ).
<verbatim>
# For Minuit2:
#For lxplus.cern.ch:
# Choose your ROOT vesion:
#ROOTHOME := /afs/cern.ch/sw/lcg/external/root/5.15.08/osx104_ppc_gcc401/root
#ROOTHOME := /afs/cern.ch/sw/lcg/external/root/5.15.08/slc3_ia32_gcc323/root
#ROOTHOME := /afs/cern.ch/sw/lcg/external/root/5.15.08/slc3_ia32_gcc323_dbg/root
#ROOTHOME := /afs/cern.ch/sw/lcg/external/root/5.15.08/slc4_ia32_gcc34/root
#ROOTHOME := /afs/cern.ch/sw/lcg/external/root/5.15.08/slc4_ia32_gcc34_dbg/root
ROOTHOME := /afs/cern.ch/sw/lcg/external/root/5.15.08/slc4_amd64_gcc34/root
#ROOTHOME := /afs/cern.ch/sw/lcg/external/root/5.15.08/slc4_amd64_gcc34_dbg/root
#ROOTHOME := /afs/cern.ch/sw/lcg/external/root/5.15.08/win32_vc71_dbg/root
</verbatim>

Once =[[http://isscvs.cern.ch/cgi-bin/cvsweb.cgi/groups/SUSY/mT2/Makefile.include?cvsroot=atlas][Makefile.include]]= is safely tailored to your system, you should be able to go to the top-directory type:
<verbatim>
make
</verbatim>
which should do the following in the following order:
   1 cd into "src" and build the library libMt2.so <br />
   1 cd into "examples" and build some simple example programs


---+ Understanding the library:  Examples


 One way to understand the library is to look at examples. Go to the examples directory  and type make:
<verbatim>
cd examples
make
</verbatim>
If the example programs built OK you should see a message reminding you to set the LD_LIBRARY_PATH variable correctly.  The suggestion made by "make" is based on the values of the variables in =[[http://isscvs.cern.ch/cgi-bin/cvsweb.cgi/groups/SUSY/mT2/Makefile.include?cvsroot=atlas][Makefile.include]]= so will probably be OK if the build was successful.

So now we can try to run some of the examples.  All the examples calculate MT2 for the same event ... the event whose 4-vectors are defined in =[[http://isscvs.cern.ch/cgi-bin/cvsweb.cgi/groups/SUSY/mT2/examples/ExampleEvent.h?cvsroot=atlas][ExampleEvent.h]]=.  You are strongly encouraged to look at the constructor of =[[http://isscvs.cern.ch/cgi-bin/cvsweb.cgi/groups/SUSY/mT2/examples/ExampleEvent.h?cvsroot=atlas][ExampleEvent]]= to understand the momenta defined therein.  The ExampleEvent declares a number of public accessor methods to the momenta of the event: not all the MT2 implementations need to know everything about the event.  For example: the first algorithm we will look at does not care about =rootS= or the z-momentum of any particle, and so accesses the event information entirely through "transverse" quantities. 

---+++  Mt2::SUSYPhys_Mt2_222_Calculator algorithm example

Here is an example of the algorithm which was used to calculate MT2 for the Rome analysis.  This algorithm has been available in the SUSYPhys package for a long while, and is copied here, with as few changes as possible, so that it may be compared with the other algorithms. It is a so-called "222" algorithm which means that it acts only on the transverse momenta of the event, assumes visible particles area massless, and pays no attention to =rootS=.  The sourcecode for this program is found in =[[http://isscvs.cern.ch/cgi-bin/cvsweb.cgi/groups/SUSY/mT2/examples/SUSYPhys_Mt2_222_Calculator_Example.cpp?cvsroot=atlas][SUSYPhys_Mt2_222_Calculator_Example.cpp]]= . Here we show what happens when you run it: 

<verbatim>
[localhost] mT2/examples > ./SUSYPhys_Mt2_222_Calculator_Example 
Going to calculate MT2 with
   ltv_Vis_A  = Lor2Vec: x =       410, y =        20, Et =   422.493, mass = 100
   ltv_Vis_B  = Lor2Vec: x =      -210, y =      -300, Et =   395.727, mass = 150
   pT_Miss    = TwoVector: x =      -200, y =       280
   invis_mass = 100
----------------------------------------------------------------------
 M_T2 : a variable for measuring masses when missing energy is expected
 If you use this code, please cite:
 (o) C.G.Lester, D.J.Summers 
         Phys.Lett.B.463:99-103 (1999) hep-ph/9906349
 (o) A.J.Barr, C.G.Lester, P.Stephens 
         J.Phys.G 29:2343-2363  (2003) hep-ph/0304226
----------------------------------------------------------------------
VariableMetricBuilder: warning: no improvement in line search  
VariableMetricBuilder: finishes without convergence.
VariableMetricBuilder: edm= 1.09485e+07 requested: 0.1
VariableMetricBuilder: Tolerance is not sufficient - edm is 43094.2 requested 0.1 continue the minimization
VariableMetricBuilder: warning: no improvement in line search  
VariableMetricBuilder: finishes without convergence.
VariableMetricBuilder: edm= 181.357 requested: 0.1
FunctionMinimum is invalid.
ANSWER: mt2 = 368.423 for Mt2::SUSYPhys_Mt2_222_Calculator algorithm
[localhost] mT2/examples >
</verbatim>


---+++  Mt2::Basic_Mt2_4441_Calculator algorithm example

This is a different example which uses an algorithm that pays attention to =rootS= and the z-momenta of particles, and is aware that visible particles may be massive.  This is a so-called "4441" algorigthm.
The sourcecode for this example is in the file =[[http://isscvs.cern.ch/cgi-bin/cvsweb.cgi/groups/SUSY/mT2/examples/Basic_Mt2_4441_Calculator_Example.cpp?cvsroot=atlas][Basic_Mt2_4441_Calculator_Example.cpp]]=.  Here is what happens when you run this example on the same event used above.

<verbatim>
[localhost] mT2/examples > ./Basic_Mt2_4441_Calculator_Example 
Going to calculate MT2 with
   p_Vis_A = (410,20,-20;422.966)
   p_Vis_B = (-210,-300,44;398.166)
   p_Vis_Other = (0,0,0;0)
   rootS = 14000
   invis_mass = 100
----------------------------------------------------------------------
 M_T2 : a variable for measuring masses when missing energy is expected
 If you use this code, please cite:
 (o) C.G.Lester, D.J.Summers 
         Phys.Lett.B.463:99-103 (1999) hep-ph/9906349
 (o) A.J.Barr, C.G.Lester, P.Stephens 
         J.Phys.G 29:2343-2363  (2003) hep-ph/0304226
----------------------------------------------------------------------
eigenvalues: 
-12.5546
14.5546
matrix forced pos-def by adding 12.5692 to diagonal
ANSWER: mt2 = 412.629 for Mt2::Basic_Mt2_4441_Calculator algorithm
[localhost] mT2/examples >
</verbatim>

Note that it produces a different answer to the one from the earlier Mt2::SUSYPhys_Mt2_222_Calculator algorithm!  That is because the SUSYPhys_Mt2_222 algorithm assumes that the visible particles are massless (or have negligible mass) which is not a valid assumption for the example event which has visible particles with ~100 Gev masses, and momenta of ~250 GeV.  See table comparing algorithms below.

---+++  Mt2::Basic_Mt2_332_Calculator algorithm example

Another simpler algorithm that can also cope correctly with visible particles of non-zero mass is the Mt2::Basic_Mt2_332_Calculator algorithm.  Here is an example of it being tested:

<verbatim>
[lxplus214] /afs/cern.ch/user/l/lester/some/working/directory/mT2/examples > ./Basic_Mt2_332_Calculator_Example
Going to calculate MT2 with
   ltv_Vis_A  = Lor2Vec: x =       410, y =        20, Et =   422.493, mass = 100
   ltv_Vis_B  = Lor2Vec: x =      -210, y =      -300, Et =   395.727, mass = 150
   pT_Miss    = TwoVector: x =      -200, y =       280
   invis_mass = 100
----------------------------------------------------------------------
 M_T2 : a variable for measuring masses when missing energy is expected
 If you use this code, please cite:
 (o) C.G.Lester, D.J.Summers 
         Phys.Lett.B.463:99-103 (1999) hep-ph/9906349
 (o) A.J.Barr, C.G.Lester, P.Stephens 
         J.Phys.G 29:2343-2363  (2003) hep-ph/0304226
----------------------------------------------------------------------
Info in <Minuit2>: VariableMetricBuilder: no improvement in line search
Info in <Minuit2>: VariableMetricBuilder: iterations finish without convergence.
Info in <Minuit2>: VariableMetricBuilder : edm = 127402
Info in <Minuit2>:             requested : edmval = 0.1
Info in <Minuit2>: VariableMetricBuilder: Tolerance is not sufficient, continue the minimization
Info in <Minuit2>: edm = 50718.1
Info in <Minuit2>: required : edmval = 0.1
Info in <Minuit2>: VariableMetricBuilder: no improvement in line search
Info in <Minuit2>: VariableMetricBuilder: iterations finish without convergence.
Info in <Minuit2>: VariableMetricBuilder : edm = 264504
Info in <Minuit2>:             requested : edmval = 0.1
Info in <Minuit2>: FunctionMinimum is invalid.
ANSWER: mt2 = 413.777 for Basic_Mt2_332 algorithm
[lxplus214] /afs/cern.ch/user/l/lester/some/working/directory/mT2/examples >
</verbatim>

---+ Validating/Testing the Library

Most users can safely ignore the files and the programs built in the =[[http://isscvs.cern.ch/cgi-bin/cvsweb.cgi/groups/SUSY/mT2/bin/?cvsroot=atlas][mT2/bin]]= directory.   This directory is mainly present for the library developers.  Amongst other things, this directory contains some old Fortran MT2 implementations which are, so far as is known, reasonably bug free, and which can therefore be used to validate the newer C++ algorithms.   The program =test2.x= whose main program is contaned in =[[http://isscvs.cern.ch/cgi-bin/cvsweb.cgi/groups/SUSY/mT2/bin/test2.cpp?cvsroot=atlas][test2.cpp]]= runs all the C++ MT2 of libMt2.so, and all the old Fortran implementations in =[[http://isscvs.cern.ch/cgi-bin/cvsweb.cgi/groups/SUSY/mT2/bin/?cvsroot=atlas][mT2/bin]]= one after the other on the same event, and compares their answers.  It is not expected that "end-users" will want to run this program, however, for completeness, here is the output of =test2.x= when run on the "standard" example event (the same event used in =[[http://isscvs.cern.ch/cgi-bin/cvsweb.cgi/groups/SUSY/mT2/examples/ExampleEvent.h?cvsroot=atlas][ExampleEvent.h]]= above):

<verbatim>
[localhost] mT2/test > ./test2.x
 ... snip ...
ANS 1 SUSYPhys_Mt2_222   = 368.423 GeV
ANS 2 Basic_Mt2_332      = 413.777 GeV
ANS 3 Basic_Mt2_4441     = 412.629 GeV
ANS 4 Summers            = 368.377 GeV
ANS 5 Lester4            = 412.628 GeV
ANS 6 Lester2            = 412.628 GeV
</verbatim>



---+ Comparison of algorithms

| *Algorithm* | *MT2 for standard example event* | *Assumptions* | *Lang* | *Dims* | *Remarks* | 
| SUSYPhys_Mt2_222 | 368.423 | visible particles are massless | C++ | 2 | This is a transcription of the method by Tovey or Hinchliffe (I forget which!) that could be found in the [[http://isscvs.cern.ch/cgi-bin/cvsweb.cgi/offline/PhysicsAnalysis/SUSYPhys/SUSYPhysUser/src/?cvsroot=atlas ][SUSYPhys]] package. Given this method's assumptions, it ought to give the same answer as the "Summers" method. |
| Basic_Mt2_332 | 413.777 | none | C++ | 2 | Identical to  SUSYPhys_Mt2_222 in all respects except that we remove the assumption that visible particles are massless.  Note that the answer generated by this algorithm is a little bit larger than the ones generated by Basic_Mt2_4441, Lester4 or Lester2 (given its assumptions it should be the same).  I suspect that this demonstrates a minor failure of the internal numerical minimisation. |
| Basic_Mt2_4441 | 412.629 | none | C++ | 2 | Not much tested.  Seemingly marginally more accurate than Basic_MT2_332 (compare with Lester4). In principle this method can make use of z-momentum information, but it is not yet clear whether that ever matters.  In all cases it ought to produce an MT2 value at least as big as that generated by Lester4 or Lester2 as this algorithm makes all the same assumptions. Method not written up anywhere - and unusual as it parametrises in terms of two invariant masses of multiparticle sub-systems. |
| Summers (222) | 368.377 | visible particles are massless | Fortran | 1 | Tried and tested and bug free.  Unfortunately only in Fortran.  Implementation is impenetrable and unmaintainable.  |
| Lester4 (332/333) | 412.628 | none | Fortran | 1 | Historical relic - but relatively well tested, and so useful for validating other methods that work on general masses.  Unfortunately only in Fortran. If visible particles are massless, "Summers" method is a tiny bit faster and more accurate, but otherwise Lester4 is a reasonable all-rounder.  Never written up in a published paper, this is nonetheless an EXTENSION of the method written up in equation (25) of TBG hep-ph/0304226 to the case where the visible particles are allowed to have DIFFERENT masses. Algorithm is internally "333 type", but C++ wrapper in [[http://isscvs.cern.ch/cgi-bin/cvsweb.cgi/groups/SUSY/mT2/bin/Summers.cpp?cvsroot=atlas][bin/Summers.cpp]] only reveals a 332 "surface".  This reduction assumes m_other=0 which is possibly inefficient given gauge freedom discoveries of 4th./5th July 2007 - which may make it into a future algorithm. Documented in Lester Lab Book 6 4th + 5th July 2007 and in Home Lab Book near centre.|
| Lester3 (332) | --- | visible particles have SAME mass | Fortran | 1 | Unfortunately only in Fortran. If visible particles are massless, "Summers" method is a tiny bit faster and more accurate.   Not suitable for use in MTGEN or other places where visible particles have significantly different masses.  Was the first implementation for identical massive visible particles to reduce the minimisation to 1 dimension.  This makes the implementation hard to follow, though the method was written up in equation (25) of TBG hep-ph/0304226. Algorithm is internally "333 type", but a C++ wrapper in [[http://isscvs.cern.ch/cgi-bin/cvsweb.cgi/groups/SUSY/mT2/bin/Summers.cpp?cvsroot=atlas][bin/Summers.cpp]] if implemented would only reveals a 332 "surface".  This reduction would assume m_other=0 which is possibly inefficient given gauge freedom discoveries of 4th./5th July 2007 - which may make it into a future algorithm.  |
| Lester2 (332/442) | 412.628 | none | Fortran | 4 | Historical relic - slightly slow but perhaps one of the simplest implementations.  Unusual in that all three components (px, py & pz) of each neutralino are free (subject to the ptmiss constraint) making this algorithm a minimisation over four dimensions. Was superseded by "Lester4". Internally Fortran implementation is 442, but C++ wrapper in [[http://isscvs.cern.ch/cgi-bin/cvsweb.cgi/groups/SUSY/mT2/bin/Summers.cpp?cvsroot=atlas][bin/Summers.cpp]] only reveals a 332 "surface". This reduction assumes that the z momenta of each of the visible particles vanishes. |

---+ Nomenclature

Short lists of number like 332, 4441, 222 in names of MT2 algorithms describe the number of degrees of freedom in the algorighm's inputs.  Reading the digits from left to right, the number of degrees of freedom and the nature of those degrees of freedom are as follows:
   * DIGIT1 is for the input DOFs of visible particle A's momentum:
      * {px,py} = 2
      * {px,py,m} or {px,py,et} = 3
      * {px,py,pz,e} = 4
   * DIGIT2 is for the input DOFs of visible particle B's momentum:
      * {px,py} = 2
      * {px,py,m} or {px,py,et} = 3
      * {px,py,pz,e} = 4
   * DIGIT3 is for the input DOF's of the total visible momentum in the event:
      * {pxmiss,pymiss} = 2
      * {pxvis,pyvis,pzvis,evis} = 4
   * DIGIT4 (optional) indicates use of rootS_max:
      * {not used} = DIGIT4 absent,
      * {used} = 1

---+ Which algorithm should I use?

If you have no real preference, for most situations *Basic_Mt2_332* will be the best algorithm to choose.  But you should make sure that it satisfies your requirements by checking the table above.

... to be continued ... this page is still being written ... Chris Lester

---+ External dependencies

From June 2007, the only external dependency is on Minuit2.  In the default build, Minuit2 is taken from an =/afs= distribution of =ROOT=.  It could be taken from a standalone (non =ROOT=) version of Minuit2 if desired.

---+ Using with ROOT

To use the library from within ROOT, follow the instructions at the top of the example ROOT macro <a href="http://isscvs.cern.ch/cgi-bin/cvsweb.cgi/groups/SUSY/mT2/mt2_Basic_332_ROOT_example.C?cvsroot=atlas">mt2_Basic_332_ROOT_example.C</a> which shows how MT2 values can be calculated from within ROOT.  This example requires the mt2 library to have first been built outside ROOT.

There is a second example ROOT macro inside <a href="http://isscvs.cern.ch/cgi-bin/cvsweb.cgi/groups/SUSY/mT2/mtgen_ROOT_example.C?cvsroot=atlas">mtgen_ROOT_example.C</a> which shows how to calculate <a href="http://arxiv.org/abs/0708.1028">MTGEN</a> from within ROOT.  This examples does not require the mt2 library to have first been built outside ROOT.

---+ Notes for Maintainers

See StransverseMassMaintainers.

<!--Add the main topic here. i.e. create some new headings as follows:!-->

<!-- MAJOR UPDATES
For significant updates to the topic, consider adding your 'signature' (beneath this editing box) !-->
---
*Major updates*:%BR%
<!--Future editors should add their signatures beneath yours!-->
-- Main.ChristopherLester - 17 Nov 2006

<!--Please add the name of someone who is responsible for this page (probably you!) so that they can be contacted if the content needs updating!-->
%RESPONSIBLE% Main.ChristopherLester %BR%
<!--Once this page has been reviewed, please add the name and the date e.g. Main.StephenHaywood - 31 Oct 2006 -->
%REVIEW% *Never reviewed*

%STOPINCLUDE%

