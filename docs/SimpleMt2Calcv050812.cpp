// Simple Mt2 Calculator based on the observation that the majority of rotation angles in the massless case are close to 180 degrees.

#include <iostream>
#include <cmath>

using namespace std;

int main()
{
// First part just sets-up all the same named variables as found in the Analytic_Mt2-2220_calculator
// Code is getting the x and y components of a, b and ptmiss from a datafile as its starting point.
// Datafile has six columns contining the following momenta: ax, ay, bx, by, ptmissx and ptmissy

    double inputaX;
    double inputaY;
    double inputbX;
    double inputbY;
    double inputptmissX;
    double inputptmissY;

    int iloop = 1;

   while (std::cin>>inputaX) {
       std::cin>>inputaY>>inputbX>>inputbY>>inputptmissX>>inputptmissY;

    const double a = sqrt((inputaX*inputaX)+(inputaY*inputaY));
    const double b = sqrt((inputbX*inputbX)+(inputbY*inputbY));
    const double magPtmiss = sqrt((inputptmissX*inputptmissX)+(inputptmissY*inputptmissY));

    const double adotp = (inputaX*inputptmissX + inputaY*inputptmissY);
    const double bdotp = (inputbX*inputptmissX + inputbY*inputptmissY);
    const double ahdotbh = cos(atan2(inputaY,inputaX)-atan2(inputbY,inputbX));

    const double adotpangle = acos (adotp / a / magPtmiss);
    const double bdotpangle = acos (bdotp / b / magPtmiss);

    const double eap = (-inputaX*inputptmissY + inputaY*inputptmissX);
    const double ebp = (-inputbX*inputptmissY + inputbY*inputptmissX);
    const double eahbh = sin(atan2(inputaY,inputaX)-atan2(inputbY,inputbX));

    const double ahdotph = adotp / a / magPtmiss;
    const double bhdotph = bdotp / b / magPtmiss;

// For the special case of ma=0, mb=0, chi=0, MT2 is *zero* whenever the ptmiss vector is "between" visA and visB, since this always admits a solution in which p and q are parallel to a and b (respectively) making MT_a = MT_b = 0.  So find coeffs of ptmiss in the a,b basis, and see if they are both positive.  If they are, return zero:

    double MT2 = 0.0;
    int noRootsMultiplier = 1;
    if (eap/eahbh >= 0 &&  -ebp/eahbh >= 0) {
        noRootsMultiplier = 0;
    }

    if (noRootsMultiplier > 0) {

// As per the Simple Mt2 section of the paper, we first need to calculate our quadratic coefficients for deltatheta

//    const double Pi = 3.1415926535897932384626433832795028841971694;
    const double Pi = acosl(-1.0L);

    const double A = (1 + ahdotbh);
    const double B = (eahbh);
    const double C = -(ebp / b / magPtmiss);
    const double D = (bhdotph);
    const double E = -(eap / a / magPtmiss);
    const double F = (ahdotph);

    const double AA = B * (b*D - a*F);
    const double BB = a * (B*E - A*F) - b * (A*D + B*C);
    const double CC = A * (a*E + b*C);

    const int rootsign = (BB >= 0) - (BB < 0);

    const double deltaTheta = (-BB + rootsign * sqrt (BB*BB - 4*AA*CC)) / (2 * AA);

    cout.precision(12);
 
// The estimated rotation angle is therefore assumed to be 180 degrees + deltatheta

    const double quadraticTheta = Pi + deltaTheta;
    double physicalTheta = 0.0;

//The SimpleMt2 quadratic shortcut is only going to be valid for rotation angles within say 90 degrees of 180 degrees, anything bigger and the quadratic root will probably be meaningless.
//To check we have something meaningful we can check to see if the rotation angle is such that p and q has ptmiss "inside" them.
//The angle must therefore be an angle that rotates p and q through a positive angle not less than the minimum of the angles between a and ptmiss and b and ptmiss, and an angle not greater than the maximum of the angles between a amd ptmiss and b and ptmiss.

//So first thing we need to do is calculate the positive angles between a and ptmiss and b and ptmiss
    double aPositiveAnglePtmiss = 0.0;
    double bPositiveAnglePtmiss = 0.0;
    const double aAnglePtmiss = atan2((inputaX*inputptmissY-inputaY*inputptmissX),(inputaX*inputptmissX + inputaY*inputptmissY));
    const double bAnglePtmiss = atan2((inputbX*inputptmissY-inputbY*inputptmissX),(inputbX*inputptmissX + inputbY*inputptmissY));

    if (aAnglePtmiss <0) {
        aPositiveAnglePtmiss = (2*Pi) + aAnglePtmiss;
    } else {
        aPositiveAnglePtmiss = aAnglePtmiss;
    }
    if (bAnglePtmiss <0) {
        bPositiveAnglePtmiss = (2*Pi) + bAnglePtmiss;
    } else {
        bPositiveAnglePtmiss = bAnglePtmiss;
    }
    double minAngle = min(aPositiveAnglePtmiss , bPositiveAnglePtmiss);
    double maxAngle = max(aPositiveAnglePtmiss , bPositiveAnglePtmiss);

//Finally we compare theta to the positive angle between a and ptmiss and b and ptmiss

    if (quadraticTheta >= minAngle && quadraticTheta <= maxAngle) {
            physicalTheta = quadraticTheta;
    } else {

//If it turns out that calculated angle from the quadratic estimation isn't correct, then we will need to guesstimate.
//As per paper, three choices ie which of minAngle, maxAngle or "halfway between" is best to use as an alternative.
//There are two different routes to deciding on the best choice: (i) ptmiss being "close to" a or b (within 45 degrees works well), or
//(ii) a and b differing by more than a certain factor (a factor of 5 works well)

    	double estimateAngle = 0.0;

    	if (adotpangle < (45.0*Pi/180.0) || bdotpangle < (45.0*Pi/180.0)) {
		if(adotpangle < bdotpangle) {
	    		estimateAngle = bPositiveAnglePtmiss;
        	} else {
            		estimateAngle = aPositiveAnglePtmiss;
		}
    	} else {
    		if (a/b > 5.0 || b/a > 5.0) {
            		if ( a > b ) {
            			estimateAngle = aPositiveAnglePtmiss;
            		} else {
            			estimateAngle = bPositiveAnglePtmiss;
            		}
    		} else {
			estimateAngle = (aPositiveAnglePtmiss + bPositiveAnglePtmiss) * 0.5;
		}
    	}
//We also need to adjust the guesstimate angle by a very small amount as being exactly
//"on the cusp" would be a little unstable  - can lead to very large/small "p_over_q" and "thingTwo" values)
        if (!(estimateAngle == 0.0)) {
            if (estimateAngle == minAngle) {
                physicalTheta = (minAngle + 0.00000001);
            } else {
                if (estimateAngle == maxAngle) {
                	physicalTheta = (maxAngle - 0.00000001);
                } else {
			physicalTheta = estimateAngle;
		}
	    }
        }
    }

//OK we now have the rotation angle giving lowest Mt2 value so let's calculate it!

    double MT2calc1 = 0.0;
    double MT2calc2 = 0.0;
    double estimatedMT2 = 0.0;
    double p_over_q = 0.0;
    double thingTwo = 0.0;

    double physicalSinTheta = sin (physicalTheta);
    double physicalCosTheta = cos (physicalTheta);

    p_over_q = -
         (( physicalCosTheta * eap + physicalSinTheta * adotp) * b )/
         (( physicalCosTheta * ebp + physicalSinTheta * bdotp) * a );

    thingTwo = 1 / (physicalCosTheta + p_over_q * (physicalCosTheta * ahdotbh + physicalSinTheta * eahbh));

    const double q = thingTwo * adotp / a;
    const double p = p_over_q * q;

    MT2calc1 = sqrt(2.0 * a * p * (1 - physicalCosTheta * ahdotbh - physicalSinTheta * eahbh));
    MT2calc2 = sqrt(2.0 * b * q * (1 - physicalCosTheta * ahdotbh + physicalSinTheta * eahbh));

//One last check just in case the guesstimate produces wildly different MT2 values on either side of the balanced eq.
//If so (ie I have used a difference of more than a factor of 200), then just choose the larger ie "sensible" value

    if (MT2calc1/MT2calc2 < 0.005 || MT2calc2/MT2calc1 < 0.005) {
	estimatedMT2 = max(MT2calc1, MT2calc2);
    } else {
    	estimatedMT2 = (MT2calc1 + MT2calc2)*0.5;
    }

    MT2 = estimatedMT2;

    }

//Simple output of the Mt2 value (ie assumes code was given an output file when it was executed

    cout<<" Run "<< iloop << " Mt2 = " << MT2 << endl;

    iloop = iloop + 1;

    }

    return 0;
}
